package main;

import java.util.ArrayList;

public class Sentence {
    int latinCount = 0;
    int digitCount = 0;
    int cyrillicCount = 0;
    int unknownCount = 0;
    ArrayList<String> words = new ArrayList<>();
    private String sentence;

    public Sentence(String sentence) {
        this.sentence = sentence;
        breakByWords(sentence);
        setWordsCount();
    }

    private void setWordsCount() {
        for (String word : words) {
            if (isLatin(word)) {
                latinCount++;
            } else if (isCyrillic(word)) {
                cyrillicCount++;
            } else if (isDigit(word)) {
                digitCount++;
            } else {
                unknownCount++;
            }
        }
    }

    public int getLatinCount() {
        return latinCount;
    }

    public int getDigitCount() {
        return digitCount;
    }

    public int getCyrillicCount() {
        return cyrillicCount;
    }

    public int getUnknownCount() {
        return unknownCount;
    }

    public ArrayList<String> getWords() {
        return words;
    }

    public void setWords(ArrayList<String> words) {
        this.words = words;
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
        breakByWords(sentence);
    }

    public static boolean isLatin(String word){
        return word.matches("^[a-zA-Z]+$");
    }
    public static boolean isCyrillic(String word){
        return word.matches("^[а-яА-Я]+$");
    }
    public static boolean isDigit(String word){
        return word.matches("^[\\d+,\\d]+$");
    }

    public  void breakByWords(String text) {
        for (String s :
                text.split("[^A-Za-zА-Яа-я\\d+,\\d+]+")) {
            if (!s.equals(",")){
                words.add(s);
            }
        }
    }
}
