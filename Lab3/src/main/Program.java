package main;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.regex.Pattern;

public class Program {

    public static String ReadFromFile(String filepath) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(filepath));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            String result = sb.toString();
            return result;
        } catch (IOException e) {
            e.printStackTrace();
            br.close();
            return null;
        }
    }

    public static void main(String [] args) throws IOException {
        String text = ReadFromFile("text.txt");
        TextWork textWork = new TextWork(text);
        textWork.printCount();

        char startWith = 's';
        System.out.println("\n SECOND WORDS IN SENTENCE STARTS WITH: "+ startWith + "\n");
        System.out.println(textWork.printSecondWordsStartWith(startWith));

        int len = 2;
        System.out.println("\n WORDS IN QUESTIONS WHICH HAS LENGTH: "+ len + "\n");
        System.out.println(textWork.printWordsInQWithLength(len));

        System.out.println("\nTEXT: \n");
        textWork.printSentences();

        System.out.println("\nTEXT AFTER REMOVING SENTENCES WISH MAX UNKNOWN WORDS: \n");
        textWork.removeSentenceWithMostUnknownWords();
        textWork.printSentences();

        System.out.println("\nADDED TYPE TO DIGITS: \n");
        textWork.addTypeToDigits();
    }
}
