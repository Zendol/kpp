package main;

import java.text.BreakIterator;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


public class TextWork {
    public String text;
    private ArrayList<Sentence> sentences = new ArrayList<>();

    public TextWork(String text){
        this.text = text;
        breakBySentences(text);
    }

    public void printCount() {
        int latinCount = 0;
        int digitCount = 0;
        int cyrillicCount = 0;
        for (Sentence sentence:
                sentences) {
            latinCount += sentence.getLatinCount();
            digitCount += sentence.getDigitCount();
            cyrillicCount += sentence.getCyrillicCount();
        }

        System.out.println("Latin: "+ latinCount);
        System.out.println("Cyrillic: "+ cyrillicCount);
        System.out.println("Digit: "+ digitCount);
    }

    public void breakBySentences(String text){
        BreakIterator iterator = BreakIterator.getSentenceInstance(Locale.UK);
        iterator.setText(text);
        int start = iterator.first();
        for (int end = iterator.next();
             end != BreakIterator.DONE;
             start = end, end = iterator.next()) {
            sentences.add(new Sentence(text.substring(start,end).trim()));
        }
    }


    public void removeSentenceWithMostUnknownWords(){
        List<Integer> listUnknownCount = new ArrayList<>();
        for (Sentence sentence:
             sentences) {
            listUnknownCount.add(sentence.getUnknownCount());
        }

        int max = Collections.max(listUnknownCount);
        sentences.removeIf(s -> s.getUnknownCount() == max);
    }

    public void addTypeToDigits(){
        ArrayList<String> sentencesWithType = new ArrayList<>();
        for (Sentence sentence : sentences){
            String newWord = "";
            for (String word : sentence.getWords()){
                newWord = word;
                if (Sentence.isDigit(word)){
                    if (word.matches("[+-]?([0-9]*[.])?[0-9]+")){
                        newWord = "(int)" + word;
                    }
                    else {
                        newWord = "(double)" + word;
                    }
                    sentencesWithType.add(sentence.getSentence().replace(word, newWord));
                }
            }
        }

        for (String sentence:
             sentencesWithType) {
            System.out.println(sentence);
        }
    }

    public void printSentences() {
        for (Sentence sentence:
             sentences) {
            System.out.println(sentence.getSentence());
        }
    }

    public String printSecondWordsStartWith(char ch){
        String resStr = "";
        for (Sentence sentence:
                sentences) {
            var words = sentence.getWords();
            if(words.size() > 1 && words.get(1).charAt(0) == ch){
                resStr += words.get(1) + " ";
            }
        }
        return resStr;
    }

    public String printWordsInQWithLength(int len){
        boolean hasQuestions = false;
        List<String> result = new ArrayList<>();
        for (Sentence sentence:
                sentences) {
            if(sentence.getSentence().trim().charAt(sentence.getSentence().length() - 1) == '?'){
                ArrayList<String> words = sentence.getWords();
                for (String word:
                        words) {
                    if(word.length() == len){
                        result.add(word);
                    }
                }
                hasQuestions = true;
            }
        }
        String res = "";
        result = result.stream().distinct().collect(Collectors.toList());
        if(result.size() > 0) {
            for (String word :
                    result) {
                res += word + " ";
            }
        } else if(!hasQuestions) {
            res = "There are no questions in the text";
        } else {
            res = "There are no words with length = "+len;
        }

        return res;
    }
}
