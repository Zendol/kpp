import main.TextWork;
import org.junit.Assert;
import org.junit.Test;

public class TestMethods {

    public TestMethods() {

    }
    @Test
    public void printSecondWordsStartWith_SentencesCorrect_ReturnsWords() {
        String str = "New sentence. One more to check! Final test? They save my life.";
        TextWork textWork = new TextWork(str);

        String res = textWork.printSecondWordsStartWith('s');
        String expectedResult = "sentence save ";

        Assert.assertEquals(expectedResult, res);
    }

    @Test
    public void printSecondWordsStartWith_NoSecondWord_ReturnsEmptyString() {
        String str = "New. One! Final? They.";
        TextWork textWork = new TextWork(str);

        String res = textWork.printSecondWordsStartWith('s');
        String expectedResult = "";

        Assert.assertEquals(expectedResult, res);
    }

    @Test
    public void printWordsInQWithLength_SentencesCorrect_ReturnsWords() {
        String str = "New sen sen one two? One more to check! Final tru test? They save my life.";
        TextWork textWork = new TextWork(str);

        String res = textWork.printWordsInQWithLength(3);
        String expectedResult = "New sen one two tru ";

        Assert.assertEquals(expectedResult, res);
    }

    @Test
    public void printWordsInQWithLength_NoWordsWithLength_ReturnsMessage() {
        String str = "New sen sen one two? One more to check! Final tru test. They save my life.";
        TextWork textWork = new TextWork(str);
        int len = 8;

        String res = textWork.printWordsInQWithLength(8);
        String expectedResult = "There are no words with length = "+len;

        Assert.assertEquals(expectedResult, res);
    }

    @Test
    public void printWordsInQWithLength_NoQuestions_ReturnsMessage() {
        String str = "New sen sen one two. One more to check! Final tru test. They save my life.";
        TextWork textWork = new TextWork(str);

        String res = textWork.printWordsInQWithLength(3);
        String expectedResult = "There are no questions in the text";

        Assert.assertEquals(expectedResult, res);
    }
}
