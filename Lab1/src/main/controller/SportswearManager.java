package main.controller;

import main.model.Brand;
import main.model.Sportswear;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class SportswearManager {
    private final ArrayList<Sportswear> sportsWears;

    public SportswearManager(ArrayList<Sportswear> sportsWears) {
        this.sportsWears = sportsWears;
    }

    public void PrintSportswear(ArrayList<Sportswear> _sportswears) {
        for (Sportswear s : _sportswears) {
            System.out.println(s);
        }
    }

    public void SearchByPrice(int from, int to) {
        ArrayList<Sportswear>res = new ArrayList<>();

        for (Sportswear s: sportsWears) {
            if(s.getPrice() >= from && s.getPrice() <= to) {
                res.add(s);
            }
        }
        PrintSportswear(res);
    }

    public void SearchByBrand(Brand brand) {
        ArrayList<Sportswear>res = new ArrayList<>();

        for (Sportswear s: sportsWears) {
            if(brand == s.getBrand()) {
                res.add(s);
            }
        }
        PrintSportswear(res);
    }

    static class PriceComparator implements Comparator<Sportswear> {
        @Override
        public int compare(Sportswear o1, Sportswear o2) {
            return Integer.compare(o1.getPrice(), o2.getPrice());
        }

    }

    class CottonPercentComparator implements  Comparator<Sportswear> {
        @Override
        public int compare(Sportswear o1, Sportswear o2) {
            if(o1.getMaterials().containsKey("cotton") && o2.getMaterials().containsKey("cotton"))
                return o1.getMaterials().get("cotton").compareTo(o2.getMaterials().get("cotton"));
            else if (o1.getMaterials().containsKey("cotton")){
                return 1;
            } else if(o2.getMaterials().containsKey("cotton")) {
                return -1;
            } else {
                return 0;
            }
        }
    }

    public void SortByPrice(boolean isDesc) {
        Collections.sort(sportsWears, new PriceComparator());
        if(isDesc){
            Collections.reverse(sportsWears);
        }
        PrintSportswear(sportsWears);
    }

    public void SortByCottonPercent(boolean isDesc) {
        Collections.sort(sportsWears, new CottonPercentComparator());
        if(isDesc){
            Collections.reverse(sportsWears);
        }
        PrintSportswear(sportsWears);
    }

    public void SortByColor(boolean isDesc) {
        Collections.sort(sportsWears, new Comparator<Sportswear>() {
            @Override
            public int compare(Sportswear o1, Sportswear o2) {
                return o1.getColor().compareToIgnoreCase(o2.getColor());
            }
        });

        if(isDesc){
            Collections.reverse(sportsWears);
        }
        PrintSportswear(sportsWears);
    }

    public void SortByModel(boolean isDesc) {
        Collections.sort(sportsWears, (Sportswear o1, Sportswear o2) -> o1.getModel().compareToIgnoreCase(o2.getModel()));

        if(isDesc){
            Collections.reverse(sportsWears);
        }
        PrintSportswear(sportsWears);
    }
}
