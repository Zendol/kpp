package main.model;

import java.util.HashMap;


public abstract class Sportswear {
    private final int price;
    private final HashMap<String,Integer> materials;
    private final Brand brand;
    private final String model;
    private final String color;

    public Sportswear(int price, HashMap<String, Integer> materials, Brand brand, String model, String color) {
        this.price = price;
        this.materials = materials;
        this.brand = brand;
        this.model = model;
        this.color = color;
    }

    public int getPrice() {
        return price;
    }

    public HashMap<String, Integer> getMaterials() {
        return materials;
    }

    public Brand getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }

    @Override
    public String toString() {
        return  "\t\tprice=" + price +
                ", \t\tmaterials=" + materials +
                ", \t\tbrand=" + brand +
                ", \t\tmodel='" + model + '\'' +
                ", \t\tcolor='" + color + '\'';
    }
}
