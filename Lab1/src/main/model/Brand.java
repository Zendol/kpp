package main.model;

public enum Brand {
    Adidas,
    Nike,
    Puma,
    NewBalance,
    Reebok
}
