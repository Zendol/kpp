package main.model.shoes;

import main.model.Brand;
import main.model.Sportswear;

import java.util.HashMap;

public abstract class Shoes extends Sportswear {
    private double size;

    public Shoes(int price, HashMap<String, Integer> materials, Brand brand, String model, String color, double size) {
        super(price, materials, brand, model, color);
        this.size = size;
    }

    @Override
    public String toString() {
        return  super.toString() + "\t\tsize=" + size;
    }
}
