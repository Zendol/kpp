package main.model.shoes;

import main.model.Brand;

import java.util.HashMap;

public class Sneakers extends Shoes{
    private boolean hasShoelace;

    public Sneakers(int price, HashMap<String, Integer> materials, Brand brand, String model, String color, double size, boolean hasShoelace) {
        super(price, materials, brand, model, color, size);
        this.hasShoelace = hasShoelace;
    }

    @Override
    public String toString() {
        return "Sneakers{" + super.toString() +
                "\t\thasShoelace=" + hasShoelace +
                '}';
    }
}
