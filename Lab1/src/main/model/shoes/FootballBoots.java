package main.model.shoes;

import main.model.Brand;

import java.util.HashMap;

public class FootballBoots extends Shoes {
    private int spikes;

    public FootballBoots(int price, HashMap<String, Integer> materials, Brand brand, String model, String color, double size, int spikes) {
        super(price, materials, brand, model, color, size);
        this.spikes = spikes;
    }

    @Override
    public String toString() {
        return "FootballBoots{" + super.toString() +
                "\t\tspikes=" + spikes +
                '}';
    }
}
