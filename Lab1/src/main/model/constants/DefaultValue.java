package main.model.constants;

import main.model.Brand;
import main.model.Sportswear;
import main.model.clothes.Jacket;
import main.model.clothes.Tracksuit;
import main.model.clothes.Trousers;
import main.model.shoes.FootballBoots;
import main.model.shoes.Sneakers;

import java.util.ArrayList;
import java.util.HashMap;

public class DefaultValue {

    public ArrayList<Sportswear> getSportswearDefaultValue() {

        ArrayList<Sportswear> sportswears = new ArrayList<>();

        sportswears.add(new Jacket(400, new HashMap<>(){{put("cotton",100);}},
                Brand.Adidas, "Samstag", "black", "M", true));
        sportswears.add(new Jacket(300, new HashMap<>(){{put("cotton",30);}},
                Brand.Nike, "NewModel", "red", "XL", false));
        sportswears.add(new Jacket(550, new HashMap<>(){{put("cotton",60);}},
                Brand.Puma, "Super", "white", "L", true));

        sportswears.add(new Tracksuit(1000, new HashMap<>(){{put("cotton",90);}},
                Brand.Reebok, "classic", "yellow", "S"));
        sportswears.add(new Tracksuit(1300, new HashMap<>(){{put("cotton",44);}},
                Brand.Nike, "ssss", "black", "L"));

        sportswears.add(new Trousers(1000, new HashMap<>(){{put("cotton",80);}},
                Brand.Adidas, "short", "pink", "XS"));
        sportswears.add(new Trousers(1300, new HashMap<>(){{put("cotton",77);}},
                Brand.NewBalance, "23-fd", "gray", "XL"));

        sportswears.add(new Sneakers(1000, new HashMap<>(){{put("leather",100);}},
                Brand.NewBalance, "newnew", "purple", 38, true));
        sportswears.add(new Sneakers(1300, new HashMap<>(){{put("dermantin",100); put("cotton",0);}},
                Brand.Puma, "truepuma", "white", 41, false));


        sportswears.add(new FootballBoots(1000, new HashMap<>(){{put("leather",100); put("cotton",0);}},
                Brand.Adidas, "dddd", "white", 38, 12));
        sportswears.add(new FootballBoots(1300, new HashMap<>(){{put("dermantin",100); put("cotton",0);}},
                Brand.Nike, "extra", "black", 41, 40));

        return sportswears;
    }

}
