package main.model.constants;

public class DefaultMessages {
    public static void printMenu() {
        System.out.println("_________________________________________");
        System.out.println("|\t Choose an operation\t\t\t\t|");
        System.out.println("|\t\t\t\t\t\t\t\t\t\t|");
        System.out.println("|\t  Operations list:\t\t\t\t\t|\n" +
                "|\t1. Print all sportswear\t\t\t|\n" +
                "|\t2. Find sportswear by parameter\t|\n" +
                "|\t3. Sort sportswear by parameter\t|\n" +
                "|\t\t\t\t\t\t\t\t\t\t|\n" +
                "|\t9. Exit\t\t\t\t\t\t\t\t|");
        System.out.println("_________________________________________");
    }


    public void printSearchParameters() {
        System.out.println("Choose a parameter");
        System.out.println(" 1. price");
        System.out.println(" 2. brand");
    }

    public void printSortParameters() {
        System.out.println("Choose a parameter");
        System.out.println(" 1. price");
        System.out.println(" 2. cottonPercent");
        System.out.println(" 3. color");
        System.out.println(" 4. model");
    }
}