package main.model.clothes;

import main.model.Brand;

import java.util.HashMap;

public class Tracksuit extends Clothes {

    public Tracksuit(int price, HashMap<String, Integer> materials, Brand brand, String model, String color, String size) {
        super(price, materials, brand, model, color, size);
    }

    @Override
    public String toString() {
        return "Tracksuit{ "+ super.toString() +" }";
    }
}
