package main.model.clothes;

import main.model.Brand;
import main.model.Sportswear;

import java.util.HashMap;

public abstract class Clothes extends Sportswear {
    private final String size;

    public Clothes(int price, HashMap<String, Integer> materials, Brand brand, String model, String color, String size) {
        super(price, materials, brand, model, color);
        this.size = size;
    }

    @Override
    public String toString() {
        return super.toString() +
                "size='" + size + '\'';
    }
}
