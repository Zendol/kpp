package main.model.clothes;

import main.model.Brand;

import java.util.HashMap;
public class Jacket extends Clothes {
    private final boolean hasZipper;

    public Jacket(int price, HashMap<String, Integer> materials, Brand brand, String model, String color, String size, boolean hasZipper) {
        super(price, materials, brand, model, color, size);
        this.hasZipper = hasZipper;
    }

    @Override
    public String toString() {
        return "Jacket{"+ super.toString() +
                "\t\thasZipper=" + hasZipper +
                '}';
    }
}
