package main.view;

import main.controller.SportswearManager;
import main.model.Brand;
import main.model.Sportswear;
import main.model.constants.DefaultMessages;
import main.model.constants.DefaultValue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class UserInteraction {
    public static final String INVALID_NUMBER = "YOU ENTERED INVALID NUMBER, PLEASE TRY AGAIN";

    ArrayList<Sportswear> defaultValues = new DefaultValue().getSportswearDefaultValue();
    SportswearManager sportswearManager = new SportswearManager(defaultValues);
    DefaultMessages defaultMessages = new DefaultMessages();

    Scanner scan = new Scanner(System.in);

    int fromUser;
    int from;
    int to;
    String tempStr;

    public void consoleInterface() {
        defaultMessages.printMenu();
        fromUser = scan.nextInt();
        if (fromUser == 1) {
            sportswearManager.PrintSportswear(defaultValues);
        } else if (fromUser == 2) {
            findByParameter();
        } else if(fromUser == 3) {
            sortByParameter();
        } else if (fromUser == 9) {
            return;
        } else {
            System.err.println(INVALID_NUMBER);
        }
        consoleInterface();
    }

    private void findByParameter() {
        defaultMessages.printSearchParameters();
        fromUser = scan.nextInt();
        if (fromUser == 1) {
            System.out.println("Enter the lowest and the highest price for sportswear");
            from = scan.nextInt();
            to = scan.nextInt();
            sportswearManager.SearchByPrice(from, to);
        } else if (fromUser == 2) {
            System.out.println("Enter the key of the Brand\n");
            for (Brand brand: Brand.values()) {
                System.out.println(brand.name());
            }
            tempStr = scan.next();
            try{
                Brand brand = Brand.valueOf(tempStr);
                if (Arrays.stream(Brand.values()).anyMatch(x -> x == brand)){
                    sportswearManager.SearchByBrand(brand);
                }
            }
            catch (Exception ex) {
                System.err.println("YOU ENTERED WRONG Brand, PLEASE TRY AGAIN");
            }
        }
        else {
            System.err.println(INVALID_NUMBER);
        }
    }

    private void sortByParameter() {
        defaultMessages.printSortParameters();
        fromUser = scan.nextInt();

        if(fromUser == 1) {
            System.out.println("Enter order: \n 1 - asc 2 - desc");
            from = scan.nextInt();
            System.out.println("Sorting by price: \n");
            sportswearManager.SortByPrice(from == 2);
        } else if(fromUser == 2) {
            System.out.println("Enter order: \n 1 - asc 2 - desc");
            from = scan.nextInt();
            System.out.println("Sorting by cottonPercent: \n");
            sportswearManager.SortByCottonPercent(from == 2);
        } else if(fromUser == 3) {
            System.out.println("Enter order: \n 1 - asc 2 - desc");
            from = scan.nextInt();
            System.out.println("Sorting by color: \n");
            sportswearManager.SortByColor(from == 2);
        } else if(fromUser == 4) {
            System.out.println("Enter order: \n 1 - asc 2 - desc");
            from = scan.nextInt();
            System.out.println("Sorting by model: \n");
            sportswearManager.SortByColor(from == 2);
        }
    }
}
