package main;

import main.models.Event;

import java.util.*;
import java.util.stream.Collectors;

public class Program {
    public static void main(String[] args) {
        FileWork fileWork = new FileWork();
        EventManager eventManager = new EventManager();
        String filename;
        ArrayList<Event> events;
        Map<Integer, List<Event>> yearEvents;

        String filename1;
        String filename2;
        ArrayList<Event> events1;
        ArrayList<Event> events2;
        List<Event> uniqueEvents1;


        while (true) {
            System.out.println(
                    "1 - Print sorted events by date grouped by year.\n" +
                            "2 - Remove events with date difference 1 day.\n" +
                            "3 - Find events which only contain file1.\n" +
                            "4 - Tuples of events with date difference 1 year.\n");


            Scanner scanner = new Scanner(System.in);
            int userInput = scanner.nextInt();
            switch (userInput) {
                case 1:
                    System.out.println("Enter filename: ");
                    scanner = new Scanner(System.in);
                    filename = scanner.nextLine();
                    events = fileWork.ReadEventsFromFile(filename);
                    yearEvents = eventManager.GetMapYearEvent(events);
                    eventManager.PrintEventsMap(yearEvents);
                    break;
                case 2:
                    System.out.println("Enter filename: ");
                    scanner = new Scanner(System.in);
                    filename = scanner.nextLine();
                    events = fileWork.ReadEventsFromFile(filename);
                    yearEvents = eventManager.GetMapYearEvent(events);
                    System.out.println("Before deleting: \n");
                    eventManager.PrintEventsMap(yearEvents);
                    yearEvents = eventManager.RemoveEventsDateDifOne(yearEvents);
                    eventManager.PrintEventsMap(yearEvents);
                    break;
                case 3:
                    System.out.print("Enter the first filename: \n");
                    scanner = new Scanner(System.in);
                    filename1 = scanner.nextLine();

                    System.out.print("Enter the second filename: \n");
                    filename2 = scanner.nextLine();

                    events1 = fileWork.ReadEventsFromFile(filename1);
                    events2 = fileWork.ReadEventsFromFile(filename2);

                    uniqueEvents1 = eventManager.GetUniqueEvents(
                            eventManager.GetMapYearEvent(events1),
                            eventManager.GetMapYearEvent(events2));

                    System.out.println("\nResult: \n");
                    eventManager.PrintEventsMap(eventManager.GetMapYearEvent(uniqueEvents1));
                    break;

                case 4:
                    System.out.print("Enter the first filename: \n");
                    scanner = new Scanner(System.in);
                    filename1 = scanner.nextLine();

                    System.out.print("Enter the second filename: \n");
                    filename2 = scanner.nextLine();

                    events1 = fileWork.ReadEventsFromFile(filename1);
                    events2 = fileWork.ReadEventsFromFile(filename2);

                    uniqueEvents1 = eventManager.GetUniqueEvents(
                            eventManager.GetMapYearEvent(events1),
                            eventManager.GetMapYearEvent(events2));

                    System.out.println("\nDifference more than one year all tuples events: ");
                    var tuples = eventManager.GetTupleEventYearGreaterOne(uniqueEvents1);
                    tuples.forEach(System.out::println);
                    break;
            }
        }
    }
}
