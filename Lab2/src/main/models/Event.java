package main.models;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


public class Event implements Comparable<Event> {
    private String name;
    private LocalDate date;

    public Event(String name, LocalDate date) {
        this.name = name;
        this.date = date;
    }

    @Override
    public String toString() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        return "'" + name + '\'' +
                ", date = " + date.format(formatter);
    }

    public String getName() {
        return name;
    }

    public LocalDate getDate() {
        return date;
    }

    public int getYear(){return date.getYear();}

    @Override
    public int compareTo(Event o) {
        return getDate().compareTo(o.getDate());
    }
}
