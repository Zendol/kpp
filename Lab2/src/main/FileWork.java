package main;

import main.models.Event;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;

public class FileWork {
    public ArrayList<Event> ReadEventsFromFile(String pathname){
        File dataFile = new File(pathname);
        try {
            var br = new BufferedReader(new FileReader(dataFile));
            ArrayList<Event> events = new ArrayList<>();
            String line;
            while((line=br.readLine())!=null)
            {
                var lines = line.split("date:");
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                LocalDate date = LocalDate.parse(lines[1].trim(), formatter);
                events.add(new Event(lines[0].trim(), date));
            }
            Collections.sort(events);
            br.close();
            return events;

        } catch (IOException ioe) {

            System.out.println(ioe.getMessage());
            return null;
        }
    }
}
