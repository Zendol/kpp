package main;

import main.models.Event;

import java.lang.reflect.Array;
import java.time.Period;
import java.util.*;
import java.util.stream.Collectors;

public class EventManager {

    public Map<Integer, List<Event>> RemoveEventsDateDifOne(Map<Integer, List<Event>> yearEvents) {
        var events = new ArrayList<>(yearEvents.values()
                .stream()
                .flatMap(List::stream)
                .collect(Collectors.toList()));

        for(int i = 0; i < events.size() - 1; i++){
            if(Math.abs(events.get(i).getDate().getDayOfYear() - events.get(i + 1).getDate().getDayOfYear()) == 1){
                events.remove(i);
                events.remove(i);
                i++;
            }
        }

        return GetMapYearEvent(events);
    }

    public  List<EventTuple> GetTupleEventYearGreaterOne(List<Event> events) {
        List<EventTuple> tuples = new ArrayList<>();
        for (int i = 0; i < events.size() - 1; i++)
            for (int j = i + 1; j < events.size(); j++)
                if (Period.between(events.get(i).getDate(), events.get(j).getDate()).getYears() >= 1)
                    tuples.add(new EventTuple(events.get(i), events.get(j)));

        return tuples;
    }

    public  List<Event> GetUniqueEvents(Map<Integer, List<Event>> e1, Map<Integer, List<Event>> e2) {
        var events1 = new ArrayList<>(e1.values())
                .stream()
                .flatMap(List::stream)
                .collect(Collectors.toList());
        var events2 = new ArrayList<>(e2.values())
                .stream()
                .flatMap(List::stream)
                .collect(Collectors.toList());

        var iterator = events1.iterator();
        while (iterator.hasNext()) {
            Event curr = iterator.next();
            for (Event e : events2) {
                if (curr.compareTo(e) == 0)
                    iterator.remove();
            }
        }

        return new ArrayList<>(events1);
    }

    public Map<Integer, List<Event>> GetMapYearEvent(List<Event> events){
        return events.stream().collect(Collectors.groupingBy(Event::getYear));
    }

    public void PrintEventsMap(Map<Integer, List<Event>> yearEvents){
        yearEvents.forEach((year, events) -> {
            System.out.println(year);
            events.forEach((event) -> {
                System.out.println("\t" + event);
            });
        });
    }

    static class EventTuple {
        private Event e1;
        private Event e2;

        public EventTuple(Event e1, Event e2) {
            this.e1 = e1;
            this.e2 = e2;
        }
        @Override
        public String toString() {
            return e1 + " - " + e2;
        }
    }
}
